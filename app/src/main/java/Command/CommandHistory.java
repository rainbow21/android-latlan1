package Command;

import java.util.Stack;

/**
 * Created by Igor on 11.03.2018.
 */

public class CommandHistory {
    private Stack history = new Stack();

    public void push(Command cmd) {
        history.push(cmd);
    }

    public Command pop() {
        return (Command)history.pop();
    }

    public Boolean isEmpty() {
        return history.isEmpty();
    }

    public int size() {
        return history.size();
    }
}
