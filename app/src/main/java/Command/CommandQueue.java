package Command;

import Queue.Queue;

public class CommandQueue {
    private Queue pull = new Queue();

    public Command get() throws InterruptedException {
        return (Command)pull.dequeue();
    }

    public void append(Command cmd) {
        pull.enqueue(cmd);
    }

    public Boolean isEmpty() {
        return pull.isEmpty();
    }
}
