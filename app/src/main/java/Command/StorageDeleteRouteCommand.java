package Command;

import root.ppo_lab1.Route;
import root.ppo_lab1.Storage;

public class StorageDeleteRouteCommand implements Command {
    private Storage storage = null;
    private int routeIndex = -1;
    private Route route = null;

    private StorageDeleteRouteCommand() {}

    @Override
    public boolean execute() {
        route = storage.getRoute(routeIndex);   // Сохранили на будущее
        storage.deleteRoute(routeIndex);        // Удалили
        return true;
    }

    @Override
    public void undo() {
        if (route == null)
            return;
        if (storage.isNew(route.getName())) {
            storage.addRoute(route);
        }
    }

    public static BuilderStorageDeleteCommand getBuilderStorageDeleteCommand() {
        return new StorageDeleteRouteCommand().new BuilderStorageDeleteCommand();
    }

    public class BuilderStorageDeleteCommand {
        public BuilderStorageDeleteCommand buildStorage(Storage storage) {
            StorageDeleteRouteCommand.this.storage = storage;
            return this;
        }
        public BuilderStorageDeleteCommand buildRoute(int rIndex) {
            routeIndex = rIndex;
            route = null;
            return this;
        }
        public StorageDeleteRouteCommand build() {
            if (storage != null && routeIndex != -1)
                return StorageDeleteRouteCommand.this;
            else
                return null;
        }
    }
}


