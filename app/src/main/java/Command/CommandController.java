package Command;

public interface CommandController {
    void exec(Command cmd);
    void redo();
    void undo();
}
