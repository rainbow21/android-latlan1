package Command;

import root.ppo_lab1.Point;
import root.ppo_lab1.Storage;

public class StorageEditPointCommand implements Command {

    private Storage storage = null;
    private Point point = null;
    private int delPointIndex = -1;
    private int delRouteIndex = -1;

    private StorageEditPointCommand() {}

    @Override
    public boolean execute() {
        String oldPoint = storage.getRoute(delRouteIndex).exportPointToString(delPointIndex);
        storage.getRoute(delRouteIndex).editPoint(delPointIndex, point);
        point = Point.getPoint(oldPoint);
        return true;
    }


    @Override
    public void undo() {
        String oldPoint = storage.getRoute(delRouteIndex).exportPointToString(delPointIndex);
        storage.getRoute(delRouteIndex).editPoint(delPointIndex, point);
        point = Point.getPoint(oldPoint);
  }

    public static BuilderStorageEditPointCommand getBuilderStorageEditPointCommand() {
        return new StorageEditPointCommand().new BuilderStorageEditPointCommand();
    }

    public class BuilderStorageEditPointCommand {
        public BuilderStorageEditPointCommand buildStorage(Storage storage) {
            StorageEditPointCommand.this.storage = storage;
            return this;
        }
        public BuilderStorageEditPointCommand buildIndex(int rIndex, int pIndex) {
            StorageEditPointCommand.this.delPointIndex = pIndex;
            StorageEditPointCommand.this.delRouteIndex = rIndex;
            return this;
        }
        public BuilderStorageEditPointCommand buildPoint(Point p) {
            StorageEditPointCommand.this.point = p;
            return this;
        }
        public StorageEditPointCommand build() {
            if (storage != null && point != null && delPointIndex != -1 && delRouteIndex != -1)
                return StorageEditPointCommand.this;
            else
                return null;
        }
    }
}
