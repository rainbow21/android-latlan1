package Command;

public class Controller implements CommandController {
    CommandHistory history;
    CommandHistory pull;
    final String TAG = "Controller";

    public Controller() {
        this.history = new CommandHistory();
        this.pull = new CommandHistory();
    }
    public void undo() {
        if (!history.isEmpty()) {
            Command cmd = history.pop();
            cmd.undo();
            pull.push(cmd);
        }
    }

    public  void restart() {
        history = new CommandHistory();
        pull = new CommandHistory();
    }
    public void exec(Command cmd) {
        if (cmd.execute())
            history.push(cmd);

    }
    public void redo() {
        if (!pull.isEmpty())
            exec(pull.pop());
    }

    public int getHistorySize() {
        return history.size();
    }
    public int getPullSize() {
        return pull.size();
    }
}
