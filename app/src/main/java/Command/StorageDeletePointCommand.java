package Command;

import root.ppo_lab1.Point;
import root.ppo_lab1.Storage;

public class StorageDeletePointCommand implements Command {
    private Storage storage = null;
    private int delRouteIndex = -1;
    private int delPointIndex = -1;
    private Point point = null;

    private StorageDeletePointCommand() {}

    @Override
    public boolean execute() {
        // Удалим из хранилища, но сохраним образец для отката
        point = storage.getRoute(delRouteIndex).deletePoint(delPointIndex);
        return true;
    }

    @Override
    public void undo() {
        storage.getRoute(delRouteIndex).appendPoint(delPointIndex, point);
    }
    public static BuilderStorageDeletePointCommand getBuilderDeletePointCommand() {
        return new StorageDeletePointCommand().new BuilderStorageDeletePointCommand();
    }

    public class BuilderStorageDeletePointCommand {
        public BuilderStorageDeletePointCommand buildStorage(Storage storage) {
            StorageDeletePointCommand.this.storage = storage;
            return this;
        }
        public BuilderStorageDeletePointCommand buildIndex(int delRoute, int delPoint) {
            delRouteIndex = delRoute;
            delPointIndex = delPoint;
            return this;
        }
        public StorageDeletePointCommand build() {
            StorageDeletePointCommand.this.point = null;
            if (storage != null)
                if (delPointIndex != -1 && delRouteIndex != -1)
                    return StorageDeletePointCommand.this;
            return null;
        }
    }
}
