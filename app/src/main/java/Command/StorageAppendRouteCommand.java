package Command;

import android.support.annotation.NonNull;

import root.ppo_lab1.Route;
import root.ppo_lab1.Storage;

public class StorageAppendRouteCommand implements Command {
    private Storage storage = null;
    private Route route = null;
    private StorageAppendRouteCommand() {}

    @Override
    public boolean execute() {
        if (storage.isNew(route.getName())) {
            storage.addRoute(route);
            return true;
        }
        return false;
    }

    @Override
    public void undo() {
        storage.deleteRoute(route);
    }

    public static BuilderStorageAppendRouteCommand getBuilderAppendRouteCommand() {
        return new StorageAppendRouteCommand().new BuilderStorageAppendRouteCommand();
    }

    public class BuilderStorageAppendRouteCommand {
        public BuilderStorageAppendRouteCommand buildStorage(Storage storage) {
            StorageAppendRouteCommand.this.storage = storage;
            return this;
        }
        public BuilderStorageAppendRouteCommand buildRoute(@NonNull Route route) {
            StorageAppendRouteCommand.this.route = route;
            return this;
        }
        public StorageAppendRouteCommand build() {
            if (storage != null &&  route != null)
                return StorageAppendRouteCommand.this;
            else
                return null;
        }
    }
}
