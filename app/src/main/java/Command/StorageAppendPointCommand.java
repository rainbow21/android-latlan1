package Command;

import root.ppo_lab1.Point;
import root.ppo_lab1.Storage;

public class StorageAppendPointCommand implements Command {
    Storage storage = null;
    Point point = null;
    int routeIndex = -1;
    private StorageAppendPointCommand() {}

    @Override
    public boolean execute() {
        storage.getRoute(routeIndex).appendPoint(point);
        return true;
    }

    @Override
    public void undo() {
        storage.getRoute(routeIndex).deletePoint(point);
    }

    public static BuilderStorageAppendPointCommand getBuilderStorageAppendCommand() {
        return new StorageAppendPointCommand().new BuilderStorageAppendPointCommand();
    }

    public class BuilderStorageAppendPointCommand {
        public BuilderStorageAppendPointCommand buildStorage(Storage storage) {
            StorageAppendPointCommand.this.storage = storage;
            return this;
        }
        public BuilderStorageAppendPointCommand buildPoint(Point p) {
            point = p;
            return this;
        }
        public BuilderStorageAppendPointCommand buildRoute(int rIndex) {
            routeIndex = rIndex;
            return this;
        }
        public StorageAppendPointCommand build() {
            if (routeIndex != -1 && point != null && storage != null)
                return StorageAppendPointCommand.this;
            else
                return null;

        }
    }
}
