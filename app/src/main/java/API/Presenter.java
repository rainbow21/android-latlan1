package API;

import Command.Command;
import Command.StorageAppendPointCommand;
import Command.StorageAppendRouteCommand;
import Command.StorageDeletePointCommand;
import Command.StorageDeleteRouteCommand;
import Command.StorageEditPointCommand;
import root.ppo_lab1.Point;
import root.ppo_lab1.Route;
import root.ppo_lab1.Storage;

public class Presenter implements PresenterAPI{
    ViewAPI view;
//    ModelAPI model;
    Storage storage;
    public Presenter(ViewAPI view, Storage model) {
        this.view = view;
        this.storage = model;
    }

    @Override
    public void deletePointFromRoute(int indexRoute, int indexPoint) {
        storage.getControllerEditPoint().exec(
                StorageDeletePointCommand.getBuilderDeletePointCommand()
                .buildStorage(storage)
                .buildIndex(indexRoute, indexPoint)
                .build()
        );
        view.updatePointView();
    }

    @Override
    public void editPointInRoute(int indexRoute, int indexPoint, Point newPoint) {
        storage.getControllerEditPoint().exec(
                StorageEditPointCommand.getBuilderStorageEditPointCommand()
                    .buildIndex(indexRoute, indexPoint)
                    .buildStorage(storage)
                    .buildPoint(newPoint)
                    .build()
        );
        view.updatePointView();
    }

    @Override
    public void appendPointToRoute(int indexRoute, Point newPoint) {
        storage.getControllerEditPoint().exec(
                StorageAppendPointCommand.getBuilderStorageAppendCommand()
                .buildStorage(storage)
                .buildRoute(indexRoute)
                .buildPoint(newPoint)
                .build()
        );
        view.updatePointView();
    }

    @Override
    public void operationWithPoint(Command cmd) {
        storage.getControllerEditPoint().exec(cmd);
        view.updatePointView();
    }

    @Override
    public void operationWithRoute(Command cmd) {
        storage.getControllerEditRoute().exec(cmd);
        view.updateRouteView();
    }

    @Override
    public void appendRoute(Route r) {
        storage.getControllerEditRoute().exec(
                StorageAppendRouteCommand.getBuilderAppendRouteCommand()
                .buildStorage(storage)
                .buildRoute(r)
                .build()
        );
        view.updateRouteView();
    }

    @Override
    public void deleteRoute(int indexRoute) {
        storage.getControllerEditRoute().exec(
                StorageDeleteRouteCommand.getBuilderStorageDeleteCommand()
                .buildStorage(storage)
                .buildRoute(indexRoute)
                .build()
        );
        view.updateRouteView();

    }

    @Override
    public void updateGraphEle(int routeIndex) {
        view.setEleGraph(storage.getEleGraph(routeIndex));
    }

    @Override
    public void undoRoute() {
        storage.getControllerEditRoute().undo();
        view.updateRouteView();
    }

    @Override
    public void redoRoute() {
        storage.getControllerEditRoute().redo();
        view.updateRouteView();
    }

    @Override
    public void undoPoint() {
        storage.getControllerEditPoint().undo();
        view.updatePointView();
    }

    @Override
    public void redoPoint() {
        storage.getControllerEditPoint().redo();
        view.updatePointView();
    }

    @Override
    public String getStringPoint(int routeIndex, int pointIndex) {
        return storage.getRoute(routeIndex).exportPointToString(pointIndex);
    }

    @Override
    public Route getRoute(int routeIndex) {
        return storage.getRoute(routeIndex);
    }

    @Override
    public int getCountRoute() {
        return storage.getCountRoute();
    }

    public Storage getModel() {
        return storage;
    }

}
