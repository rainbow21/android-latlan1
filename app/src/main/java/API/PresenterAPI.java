package API;

import java.util.List;

import Command.Command;
import root.ppo_lab1.Point;
import root.ppo_lab1.Route;
import root.ppo_lab1.Storage;

public interface PresenterAPI {
    // Point //
    void deletePointFromRoute(int indexRoute, int indexPoint);
    void editPointInRoute(int indexRoute, int indexPoint, Point newPoint);
    void appendPointToRoute(int indexRoute, Point newPoint);

    void operationWithPoint(Command cmd);
    void operationWithRoute(Command cmd);

    void updateGraphEle(int routeIndex);

    // Route //
    void appendRoute(Route r);
    void deleteRoute(int indexRoute);

    // Undo/Redo
    void undoRoute();
    void redoRoute();
    void undoPoint();
    void redoPoint();

    String  getStringPoint(int routeIndex, int pointIndex);
    Route   getRoute(int routeIndex);
    int     getCountRoute();
}
