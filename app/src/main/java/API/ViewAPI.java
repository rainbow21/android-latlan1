package API;

import com.jjoe64.graphview.series.Series;

public interface ViewAPI {
    void setEleGraph(Series s);
    void updatePointView();
    void updateRouteView();

    void listItemClick(int i);
}
