package root.ppo_lab1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class CreateRouteActivity extends AppCompatActivity implements View.OnClickListener {

    Button buttonOK;
    Button buttonCancel;
    EditText input;
    EditText inputDesc;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_route);

        buttonOK = findViewById(R.id.buttonOK);
        buttonCancel = findViewById(R.id.buttonCancel);
        input = findViewById(R.id.inputName);
        inputDesc = findViewById(R.id.inputDesc);

        buttonOK.setOnClickListener(this);
        buttonCancel.setOnClickListener(this);
        buttonOK.setEnabled(false);

        input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                buttonOK.setEnabled(!input.getText().toString().isEmpty());
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonOK:
                Intent data = new Intent();
                data.putExtra(getResources().getString(R.string.nameRoute), input.getText().toString());
                data.putExtra(getResources().getString(R.string.descRouteIntent), inputDesc.getText().toString());
                setResult(RESULT_OK, data);
                finish();
                break;
            case R.id.buttonCancel:
                setResult(RESULT_CANCELED);
                finish();
                break;
        }
    }
}
