package root.ppo_lab1;

import android.widget.Adapter;
import android.widget.ArrayAdapter;

public class PointView {
    private Route route;
    private ArrayAdapter adapter;

    public PointView(Adapter adapter, Route route) {
        this.route = route;
        this.adapter = (ArrayAdapter)adapter;
    }

    public void update() {
        adapter.clear();
        int n = route.getSize();
        for (int i = 0; i < n; i++)
            adapter.add(route.exportPointToString(i));
        adapter.notifyDataSetChanged();
    }
}
