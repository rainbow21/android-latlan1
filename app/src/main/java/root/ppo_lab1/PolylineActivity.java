package root.ppo_lab1;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.series.Series;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import API.Presenter;
import API.ViewAPI;
import Dialog.OpenFileDialog;
import Dialog.SetDirDialog;

public class PolylineActivity extends AppCompatActivity implements View.OnClickListener, ViewAPI {

    private EditText inputFileName;
    private TextView viewPath;
    private TextView viewPolyline;
    private Button buttonOK;
    private String polyline = null;
    private boolean correctName = false;
    private boolean correctPath = false;
    private Presenter presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_polyline);
        presenter = new Presenter(this, App.storage());
        int routeIndex = getIntent().getIntExtra(getResources().getString(R.string.routeIndex), -1);
        polyline = Decoder.getPolyLine(presenter.getRoute(routeIndex));
        viewPolyline = findViewById(R.id.viewPolyline);
        viewPolyline.setText(polyline);
        viewPolyline.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                ClipboardManager manager = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
                ClipData clipData = ClipData.newPlainText("", viewPolyline.getText().toString());
                manager.setPrimaryClip(clipData);
                Toast.makeText(PolylineActivity.this, getResources().getString(R.string.copyed), Toast.LENGTH_SHORT).show();
                return true;
            }
        });

        inputFileName = findViewById(R.id.inputFileName);
        viewPath = findViewById(R.id.viewPath);
        viewPath.setEnabled(false);

        // Привязка кнопок
        findViewById(R.id.buttonPath).setOnClickListener(this);
        findViewById(R.id.buttonCancel).setOnClickListener(this);
        buttonOK = findViewById(R.id.buttonOK);
        buttonOK.setEnabled(false);
        buttonOK.setOnClickListener(this);

        inputFileName.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void afterTextChanged(Editable editable) {
                correctName = !inputFileName.getText().toString().equals("");
                buttonOK.setEnabled(correctName&&correctPath);
            }
        });
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.buttonCancel:
                setResult(RESULT_CANCELED);
                finish();
                break;
            case R.id.buttonOK:
                Intent data = new Intent();
                data.putExtra(getResources().getString(R.string.fileName), inputFileName.getText().toString() + getResources().getString(R.string.polyExpansion));
                data.putExtra(getResources().getString(R.string.filePath), viewPath.getText().toString());
                setResult(RESULT_OK, data);
                savePolyline();
                finish();
                break;
            case R.id.buttonPath:
                SetDirDialog setDirDialog = new SetDirDialog(this);
                setDirDialog.setFileter(".*\\.*");
                setDirDialog.setOpenDialogListener(new OpenFileDialog.OpenDialogListener() {
                    @Override
                    public void OnSelectedFile(String fileName) {
                        viewPath.setText(fileName);
                        correctPath = true;
                        buttonOK.setEnabled(correctName&&correctPath);
                    }
                });
                setDirDialog.show();
                break;
        }
    }

    private void savePolyline() {
        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            Toast.makeText(this, "Нет доступа к памяти", Toast.LENGTH_LONG).show();
            return;
        }

        File path = new File(viewPath.getText().toString());
        File file = new File(path, inputFileName.getText().toString()+getResources().getString(R.string.polyExpansion));
        try {
            BufferedWriter write = new BufferedWriter(new FileWriter(file));
            write.write(polyline);
            write.close();
        } catch (IOException e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void setEleGraph(Series s) {
    }

    @Override
    public void updatePointView() {
    }

    @Override
    public void updateRouteView() {
    }

    @Override
    public void listItemClick(int i) {
    }
}