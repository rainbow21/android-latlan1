package root.ppo_lab1;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBHelper extends SQLiteOpenHelper {

    private static int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "IgorDB";

    private final String KEY_ID = "id";
    private final String KEY_LAT = "LAT";
    private final String KEY_LON = "LON";
    private final String KEY_ELE = "ELE";

    public String getColLatName() {
        return KEY_LAT;
    }
    public String getColLonName() {
        return KEY_LON;
    }
    public String getColEleName() {
        return KEY_ELE;
    }

    private final String TAG = "DB";

    public DBHelper(Context context)
    {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    public void createTable(String tableName) {
        this.getWritableDatabase().execSQL(
                "CREATE TABLE " + tableName
                        + "(" + KEY_ID + " INTEGER PRIMARY KEY,"
                        + KEY_LAT + " REAL,"
                        + KEY_LON + " REAL,"
                        + KEY_ELE + " REAL" + ")"
        );
        Log.d(TAG, "Create table: " + tableName);
    }

    public void appendPoint(String tableName, Point p) {
        ContentValues values = new ContentValues();
        values.put(KEY_LAT, p.getLat());
        values.put(KEY_LON, p.getLon());
        values.put(KEY_ELE, p.getEle());
        getWritableDatabase().insert(tableName, null, values);
        Log.d(TAG, "Append to " + tableName);
    }

    // Пересоздать таблицу
    public void reBuild(String tableName) {
        getWritableDatabase().execSQL("DROP TABLE IF EXISTS " + tableName);
        createTable(tableName);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
    }
    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
    }
}
