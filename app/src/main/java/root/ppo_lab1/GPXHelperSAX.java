package root.ppo_lab1;

import android.util.Log;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;

public class GPXHelperSAX extends DefaultHandler {
    private final String TAG = "SAX";
    private final String TRK = "trk";
    private final String WPT = "wpt";
    private final String GPX = "gpx";
    private final String NAME = "name";
    private final String DESC = "desc";
    private final String TRKSEG = "trkseg";
    private final String ELE = "ele";
    private final String TRKPT = "trkpt";

    private final String AttributeGPX_creator = "creator";
    private final String PointAttt_LAT = "lat";
    private final String PointAttt_LON = "lon";

    private String curElement;
    private Point curPoint;

    private String routeName = null;
    private boolean flgTRK = false;
    private ArrayList point = new ArrayList();
    private String desc = null;

    public ArrayList getPoints() {
        return point;
    }
    public String getRouteName() {
        return routeName;
    }
    public String getDesc() {
        return desc;
    }

    @Override
    public void startDocument() throws SAXException {
        super.startDocument();
        // Начало обработки документа
        Log.d(TAG, "Start parsing GPX...");
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        curElement = qName;
        switch (curElement) {
            case GPX:
                Log.d(TAG, attributes.getValue(AttributeGPX_creator));
                Log.d(TAG, "START GPX\n\n");
                break;
            case TRKPT:
                String s1 = attributes.getValue(PointAttt_LAT);
                String s2 = attributes.getValue(PointAttt_LON);
                Double d1 = Double.valueOf(s1);
                Double d2 = Double.valueOf(s2);
                curPoint = new Point(d1,d2);
                break;
            case TRK:
                flgTRK = true;
                break;
        }
    }

    @Override
    public void characters(char[] ch, int start, int length){
        if (curElement.equals(ELE)) {
            String s = new String(ch, start, length);
            s = s.trim().replaceAll(" +", " ");
            if (curPoint != null)
                curPoint.setEle(Double.valueOf(s));
            curElement = "";
        }
        if (flgTRK) {
            if (curElement.equals(NAME) && routeName == null)
                routeName = new String(ch, start, length);
            if (curElement.equals(DESC) && desc == null) {
                desc = new String(ch, start, length);
            }
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equals(TRKPT))
            point.add(curPoint);
    }

}
