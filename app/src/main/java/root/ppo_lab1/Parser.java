package root.ppo_lab1;

import android.util.Log;
import android.widget.Toast;

import org.xml.sax.InputSource;
import org.xml.sax.XMLReader;

import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

public class Parser {
    final static String TAG = "Parser";

    public static Route PolylineParser(String fileName) {
        Route route = null;
        try {
            BufferedReader reader = new BufferedReader(new FileReader(fileName));
            char c[] = fileName.toCharArray();
            String name = getFileName(fileName);
            String polyline = reader.readLine();
            route = Decoder.getRoute(polyline);
            route.rename(name);
            route.setDesc(name);


        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
        }
        return route;
    }

    public static Route SAXParser(String fileName) {
        Route route = null;
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();
            XMLReader reader = saxParser.getXMLReader();
            GPXHelperSAX gpxHelperSAX = new GPXHelperSAX();
            reader.setContentHandler(gpxHelperSAX);
            FileReader fileReader = new FileReader(fileName);
            reader.parse(new InputSource(fileReader));
            fileReader.close();

            ArrayList p = gpxHelperSAX.getPoints();
            String name = gpxHelperSAX.getRouteName();
            route = new Route(name, p);
            String desc = gpxHelperSAX.getDesc();
            route.setDesc(desc);
        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
        }
        return route;
    }

    private static String getFileName(String path) {
        char c[] = path.toCharArray();
        int begin;
        int end;
        for (begin = c.length-1; c[begin] != '/'; begin--)
            ;
        for (end = c.length-1; c[end] != '.'; end--)
            ;
        return String.valueOf(c, ++begin, end-begin);
    }
}
