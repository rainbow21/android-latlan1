package root.ppo_lab1;

import java.util.Locale;


public class Point {

    public Point setLat(Double lat) {
        this.lat = lat;
        return this;
    }

    public Point setLon(Double lon) {
        this.lon = lon;
        return this;
    }

    public Point setEle(Double ele) {
        this.ele = ele;
        return this;
    }

    private Double lat, lon;
    private Double ele;
    public Point(Double lat, Double lon) {
        this.lat = lat;
        this.lon = lon;
        this.ele = null;
    }
    public Point(Double lat, Double lon, Double ele) {
        this.lat = lat;
        this.lon = lon;
        this.ele = ele;
    }

    public static Point getPoint(String s) {
        s = getFormatForPoint(s);
        String sa[] = s.split(" ");
        Double d = null;
        if (sa.length > 2)
            d = new Double(sa[2]);

        return new Point(new Double(sa[0]), new Double(sa[1]), d);
    }
    public static String getFormatForPoint(String s) {
        return s.trim().replaceAll(" +", " ");
    }

    public Double getLat() {
        return lat;
    }

    public Double getLon() {
        return lon;
    }

    public Double getEle() {
        return ele;
    }

    @Override
    public String toString() {
        if (ele != null)
            return String.format(Locale.ENGLISH,"%9.5f %9.5f %9.5f", lat, lon, ele);
        else
            return String.format(Locale.ENGLISH,"%9.5f %9.5f", lat, lon);
    }
}
