package root.ppo_lab1;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.series.Series;

import API.Presenter;
import API.ViewAPI;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PointActivity extends AppCompatActivity implements  ViewAPI {
    @BindView(R.id.list)
    ListView listView;

    @Override
    public void setEleGraph(Series s) {
    }

    @Override
    public void updatePointView() {
        pointView.update();
    }

    @Override
    public void updateRouteView() {
    }
    @Override
    public void listItemClick(int i) {
    }

    PointView pointView;
    ArrayAdapter<String> adapter;
    int selectedIndex = -1;
    int routeIndex = -1;
    String tableName = null;
    final int REQUEST_EDIT = 1;
    final int REQUEST_APPEND = 2;
    private Presenter presenter;

    @OnClick(R.id.buttonAppendPoint)
    public void appendPoint(Button button) {
        try {
            Intent intent = new Intent(PointActivity.this, EditPointActivity.class);
            intent.putExtra(getResources().getString(R.string.routeIndex), routeIndex);
            intent.putExtra(getResources().getString(R.string.pointIndex), selectedIndex);
            startActivityForResult(intent, REQUEST_APPEND);
        } catch (Exception e) {
            Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
        }
    }

    @OnClick(R.id.undo)
    public void undoPoint(ImageButton button) {
        presenter.undoPoint();
    }

    @OnClick(R.id.redo)
    public void redoPoint(ImageButton button) {
        presenter.redoPoint();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_route);
        ButterKnife.bind(this);
        presenter = new Presenter(this, App.storage());

        // Маршрут, с которым работаем
        routeIndex = getIntent().getIntExtra(getResources().getString(R.string.routeIndex), -1);
        // Имя таблицы, где хранится маршрут
        tableName = getIntent().getStringExtra(getResources().getString(R.string.tableName));

        // Создание заголовка
        ((TextView)findViewById(R.id.title)).setText(getIntent().getStringExtra(getResources().getString(R.string.nameRoute)));

        // Настройка ListView
        adapter = new ArrayAdapter<>(this, R.layout.list_item);
        listView.setAdapter(adapter);
        pointView = new PointView(adapter, presenter.getRoute(routeIndex));

        constructListPoint();
        registerForContextMenu(listView);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.edit_context_menu, menu);

        AdapterView.AdapterContextMenuInfo adapter = ((AdapterView.AdapterContextMenuInfo)menuInfo);
        selectedIndex = adapter.position;
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.edit:
                try {
                    Intent intent = new Intent(PointActivity.this, EditPointActivity.class);
                    intent.putExtra(getResources().getString(R.string.routeIndex), routeIndex);
                    intent.putExtra(getResources().getString(R.string.pointIndex), selectedIndex);
                    startActivityForResult(intent, REQUEST_EDIT);
                } catch (Exception e) {
                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
                }
                return true;
            case R.id.delete:
                presenter.deletePointFromRoute(routeIndex, selectedIndex);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }

    private void constructListPoint() {
        DBHelper helper = new DBHelper(this);
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor cursor = db.query(tableName, null, null, null, null, null, null);
        if (cursor.moveToFirst()) {
            int colLat = cursor.getColumnIndex(helper.getColLatName());
            int colLon = cursor.getColumnIndex(helper.getColLonName());
            int colEle = cursor.getColumnIndex(helper.getColEleName());
            do {
                Point p = new Point(cursor.getDouble(colLat),
                                    cursor.getDouble(colLon));
                String ele = cursor.getString(colEle);
                if (ele != null)
                    p.setEle(Double.valueOf(ele));
                adapter.add(p.toString());
            } while (cursor.moveToNext());
        } else {
            Toast.makeText(this, "Нет данных!", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_EDIT:
                processingResultFromEdit(resultCode, data);
                break;
            case REQUEST_APPEND:
                processingResultFromAppend(resultCode, data);
                break;
            default:
                Toast.makeText(this, getResources().getString(R.string.unkownReques), Toast.LENGTH_SHORT).show();
        }
    }

    // Обработка результата после редактирования точки
    private void processingResultFromEdit(int resultCode, Intent data) {
        switch (resultCode) {
            case RESULT_OK:
                Toast.makeText(this, data.getStringExtra(getResources().getString(R.string.returnNewCoord)), Toast.LENGTH_SHORT).show();
                Point p = Point.getPoint(data.getStringExtra(getResources().getString(R.string.returnNewCoord)));
                presenter.editPointInRoute(routeIndex, selectedIndex, p);
                break;
            case RESULT_CANCELED:
                Toast.makeText(this, getResources().getString(R.string.cancelResult), Toast.LENGTH_SHORT).show();
                break;
            default:
                Toast.makeText(this, getResources().getString(R.string.unkownResult), Toast.LENGTH_SHORT).show();
        }
    }
    // Обработка результата после добавления точки
    private void processingResultFromAppend(int resultCode, Intent data) {
        switch (resultCode){
            case RESULT_OK:
                Point p = Point.getPoint(data.getStringExtra(getResources().getString(R.string.returnNewCoord)));
                presenter.appendPointToRoute(routeIndex, p);
                break;
            case RESULT_CANCELED:
                Toast.makeText(this, getResources().getString(R.string.cancelResult), Toast.LENGTH_SHORT).show();
            default:
                Toast.makeText(this, getResources().getString(R.string.unkownResult), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        presenter.getModel().getControllerEditPoint().restart();
    }
}
