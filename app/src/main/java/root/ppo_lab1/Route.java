package root.ppo_lab1;

import com.google.android.gms.maps.model.LatLng;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

public class Route {
    private String name;
    private ArrayList<Point> point;
    private String desc = null;
    private Time time;

    private final String NO_NAME = "NoName";
    public Route(String name, Point p[]) {
        if (name != null)
            this.name = name;
        else
            this.name = NO_NAME;

        int n;
        if (p != null)
            n = p.length;
        else
            n = 0;

        point = new ArrayList<Point>();
        for (int i = 0; i < n; i++)
            point.get(i).setEle(p[i].getEle())
            .setLat(p[i].getLat())
            .setLon(p[i].getLon());
        Calendar calendar = Calendar.getInstance();
        time = new Time(calendar.getTimeInMillis());
    }

    public void rename(String name) {
        this.name = name;
    }

    public Route(String name, ArrayList<Point> p ) {
        if (name != null)
            this.name = name;
        else
            this.name = NO_NAME;

        if (p != null)
            point = p;
        else
            point = new ArrayList<>();
        Calendar calendar = Calendar.getInstance();
        time = new Time(calendar.getTimeInMillis());
    }

    public Time getTime() {
        return time;
    }
    public String getName() {
        return name;
    }
    public int  getSize() {
        return point.size();
    }


    public void setDesc(String d) {
        desc = d;
    }
    public String getDesc() {
        return desc;
    }

    public String exportPointToString(int indexPoint) {
        if (indexPoint < 0 || indexPoint >= point.size())
            return null;
        String str;
        Point p = point.get(indexPoint);
        str = String.format(Locale.ENGLISH,"%9.5f" + " " +"%9.5f", point.get(indexPoint).getLat(), point.get(indexPoint).getLon());
        if (point.get(indexPoint).getEle() != null)
            str += String.format(Locale.ENGLISH," " + "%9.5f", point.get(indexPoint).getEle());
        return str;
    }

    public void exportToDB(DBHelper helper, String tableName) {
        int n = point.size();
        for (int i = 0; i < n; i++)
            helper.appendPoint(tableName, point.get(i));
    }

    public Point getPoint(int index) {
        return point.get(index);
    }

    public boolean deletePoint(Point p) {
        return point.remove(p);
    }
    public Point deletePoint(int delPoint) {
        return point.remove(delPoint);
    }
    public void appendPoint(int index, Point p) {
        point.add(index, p);
    }
    public void appendPoint(Point p) {
        point.add(p);
    }
    public void editPoint(int index, Point p) {
        point.get(index)
                .setLat(p.getLat())
                .setLon(p.getLon())
                .setEle(p.getEle());
    }
    public boolean isReadyEle() {
        int n = point.size();
        if (n == 0)
            return false;
        for (int i = 0; i < n; i++)
            if (point.get(i).getEle() == null)
                return false;
        return true;
    }
    public Double getProcEle() {
        int n = point.size();
        int countEle = 0;
        for (int i = 0; i < n; i++)
            if (point.get(i).getEle() != null)
                countEle++;
        return countEle*100.0/n;
    }
    public ArrayList<Double> getEleGraph() {
        if (!isReadyEle()) {
            if (getProcEle() <= 50)
                return new ArrayList<>();
            int n = point.size();
            int index;
            for (index = 0; point.get(index).getEle() == null; index++)
                ;

            // Точно не null
            Double tempEle = point.get(index).getEle();
            for (int i = index; i < n; i++)
                if (point.get(i).getEle() != null)
                    tempEle = point.get(i).getEle();
                else
                    point.get(i).setEle(tempEle);

            // Теперь в обратную сторону
            for (index = n-1; index >= 0 && point.get(index).getEle() != null; index--)
                ;

            // Здесь не может быть ошибки, т.к. между "дырками" точно есть высоты
            tempEle = point.get(index + 1).getEle();
            for (int i = index; i >= 0; i--)
                point.get(i).setEle(tempEle);

        }

        Double len = Decoder.getLen(this);
        // Подсчитать расстояние между двумя точками
        ArrayList<Double> list = new ArrayList<>();
        // Процент от общего пути
        ArrayList<Double> listProc = new ArrayList<>();

        int n = point.size();
        for (int i = 0; i < n-1; i++) {
            Point p1 = point.get(i);
            Point p2 = point.get(i+1);
            LatLng pl1 = new LatLng(p1.getLat(), p1.getLon());
            LatLng pl2 = new LatLng(p2.getLat(), p2.getLon());
            list.add(Decoder.getLen(pl1, pl2));
            listProc.add(list.get(i) / len * 100);
        }

        // Создать график высот на 100 элементов
        double temp = 0;
        n = listProc.size();
        long t_old = 0;

        ArrayList<Double> ele = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            temp += listProc.get(i);
            long t = Math.round(temp);
            for (long j = 0; j < t - t_old; j++)
                ele.add(point.get(i).getEle());
            t_old = t;
        }
        return ele;
    }
}
