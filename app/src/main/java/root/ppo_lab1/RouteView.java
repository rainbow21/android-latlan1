package root.ppo_lab1;

import android.widget.Adapter;
import android.widget.ArrayAdapter;

import API.Presenter;

public class RouteView {
    private ArrayAdapter adapter;
    private Presenter presenter;

    public RouteView(Adapter adapter, Presenter presenter) {
        this.adapter = (ArrayAdapter)adapter;
        this.presenter = presenter;
    }

    public void update() {
        adapter.clear();
        int n = presenter.getCountRoute();
        for (int i = 0; i < n; i++)
            adapter.add(presenter.getRoute(i).getName());
    }
}
