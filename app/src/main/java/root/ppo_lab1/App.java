package root.ppo_lab1;

import android.app.Application;

public class App extends Application {
    private static Storage storage;

    @Override
    public void onCreate() {
        super.onCreate();
        storage = new Storage();
    }

    public static Storage storage() {
        return storage;
    }
}
