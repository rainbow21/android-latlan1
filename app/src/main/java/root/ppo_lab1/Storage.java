package root.ppo_lab1;

import android.widget.ArrayAdapter;

import com.google.android.gms.maps.model.LatLng;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.ArrayList;
import java.util.List;

import Command.Controller;

public class Storage{
    private List<Route> route;
    private Controller controllerEditPoint;
    private Controller controllerEditRoute;

    final String TAG = "STORAGE";
    public Storage() {
        controllerEditPoint = new Controller();
        controllerEditRoute = new Controller();
        route = new ArrayList<>();
    }



    // Добавляемое имя вляется уникальным
    public Boolean isNew(String name) {
        for (int i = 0; i  < route.size(); i++)
            if (route.get(i).getName().equals(name))
                return false;
        return true;
    }

    // Пути
    public void addRoute(Route r) {
        route.add(r);
    }
    public void deleteRoute(Route r) {
        route.remove(r);
    }
    public void deleteRoute(int i) {
        route.remove(i);
    }

    public String[] exportRouteAsString(int indexRoute) {
        int n = route.get(indexRoute).getSize();
        String str[] = new String[n];
        for (int i = 0; i < n; i++)
            str[i] = route.get(indexRoute).exportPointToString(i);
        return str;
    }
    public String getRouteName(int indexRoute) {
        return route.get(indexRoute).getName();
    }
    public Route getRoute(int indexRoute) {
        return route.get(indexRoute);
    }
    public int  getCountRoute() {
        return route.size();
    }

    public Controller getControllerEditPoint() {
        return controllerEditPoint;
    }
    public Controller getControllerEditRoute() {
        return controllerEditRoute;
    }

    public LineGraphSeries getEleGraph(int routeIndex) {
        ArrayList<Double> ele = route.get(routeIndex).getEleGraph();
        LineGraphSeries s = new LineGraphSeries();
        int n = ele.size();
        for (int i = 0; i < n; i++)
            s.appendData(new DataPoint(i, ele.get(i)), true, 200);
        return s;
    }
}
