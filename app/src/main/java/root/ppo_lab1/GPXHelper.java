package root.ppo_lab1;

import android.widget.TextView;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class GPXHelper {
    Document doc;
    final String TAG = "GPXHelper";
    final String NAME = "name";
    final String TRK = "trk";
    public GPXHelper(Document d)
    {
        doc = d;
    }

    public void viewTreeDocument(TextView v) {
        Node root = doc.getDocumentElement();
        if (root.hasChildNodes()) {
            NodeList nodes = root.getChildNodes();
            for (int i = 0; i < nodes.getLength(); i++) {
                Node node = nodes.item(i);
                if (node.getNodeType() != Node.TEXT_NODE) {
                    v.append(node.getNodeName() + "\n");
                    if (node.hasChildNodes())
                        viewChildsNode(node.getChildNodes(), v);
                }
            }
        }
    }

    private void viewChildsNode(NodeList list, TextView v)
    {
        for (int i = 0; i < list.getLength(); i++)
            if (list.item(i).getNodeType() != Node.TEXT_NODE)
                v.append(list.item(i).getNodeName() + "\n");
    }

    public NodeList getNodes(String name) {
        return doc.getElementsByTagName(name);
    }

    public String getNameRoute() {
        NodeList trkList = getNodes(NAME);
        for (int i = 0; i < trkList.getLength(); i++) {
            Node node = trkList.item(i);
            if (node.getParentNode().getNodeName().equals(TRK))
                return node.getTextContent();
        }
        return null;
    }
}
