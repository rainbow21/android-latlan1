package root.ppo_lab1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.jjoe64.graphview.series.Series;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import API.Presenter;
import API.ViewAPI;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

public class EditPointActivity extends AppCompatActivity implements ViewAPI {
    Presenter presenter;
    int routeIndex = -1;
    int pointIndex = -1;
    boolean ele = false;

    Pattern p = Pattern.compile("^(0$|-?[1-9]\\d*(.\\d*[0-9]$)?|-?0.\\d*[0-9])$");
    @BindView(R.id.editLat)     EditText latEdit;
    @BindView(R.id.editLon)     EditText lonEdit;
    @BindView(R.id.editEle)     EditText eleEdit;
    @BindView(R.id.chB) CheckBox chBox;
    @OnCheckedChanged(R.id.chB)
    public void checkedChange(boolean b) {
        ele = b;
        updateVisibleEle();
    }

    @OnClick(R.id.enter)
    public void enterEditPoint(Button button) {
        Matcher mLat = p.matcher(latEdit.getText().toString());
        Matcher mLon = p.matcher(lonEdit.getText().toString());
        Matcher mEle = p.matcher(eleEdit.getText().toString());
        if (mLat.find() && mLon.find() && mEle.find())
            exitOK();
        else
            Toast.makeText(this, "Некорректный ввод!", Toast.LENGTH_SHORT).show();
    }

    @OnClick(R.id.cancel)
    public void exitCancel(Button button) {
        setResult(RESULT_CANCELED);
        finish();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_point);
        ButterKnife.bind(this);

        presenter = new Presenter(this, App.storage());

        routeIndex = getIntent().getIntExtra(getResources().getString(R.string.routeIndex), -1);
        pointIndex = getIntent().getIntExtra(getResources().getString(R.string.pointIndex), -1);
        String s;
        if (pointIndex != -1)
            s = presenter.getStringPoint(routeIndex, pointIndex);
        else
            s = getResources().getString(R.string.defaultInitialization);
        fillEdit(Point.getFormatForPoint(s).split(" "));
    }

    private void fillEdit(String str[]) {
        latEdit.setText(str[0]);
        lonEdit.setText(str[1]);
        eleEdit.setText("0.0");
        ele = str.length > 2;
        chBox.setChecked(ele);

        if (ele) {
            eleEdit.setText(str[2]);
        } else {
            eleEdit.setVisibility(View.INVISIBLE);
        }
    }
    // Обновление видимости "Ele"
    private void updateVisibleEle() {
        if (ele)
            eleEdit.setVisibility(View.VISIBLE);
        else
            eleEdit.setVisibility(View.INVISIBLE);
    }

    private void exitOK() {
        Intent data = new Intent();
        data.putExtra(getResources().getString(R.string.returnNewCoord), createResultString());
        setResult(RESULT_OK, data);
        finish();
    }

    private String createResultString() {
        String s = latEdit.getText().toString() + " " + lonEdit.getText().toString();
        if (chBox.isChecked())
            s += " " + eleEdit.getText().toString();

        return s;
    }

    @Override
    public void setEleGraph(Series s) {
    }
    @Override
    public void updatePointView() {
    }
    @Override
    public void updateRouteView() {
    }

    @Override
    public void listItemClick(int i) {
    }
}
