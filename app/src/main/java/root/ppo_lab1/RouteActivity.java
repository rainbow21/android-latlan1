package root.ppo_lab1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.Viewport;
import com.jjoe64.graphview.series.Series;

import java.util.Locale;

import API.Presenter;
import API.ViewAPI;
import Dialog.OpenFileDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;

public class RouteActivity extends AppCompatActivity implements ViewAPI {
    @BindView(R.id.list)
    protected ListView listView;

    @OnItemClick(R.id.list)
    public void listItemClick(int i) {
        if (i < 0) {
            graphView.setVisibility(View.INVISIBLE);
            lenRoute.setText("");
            descRoute.setText("");
            return;
        }

        String desc = presenter.getRoute(i).getDesc();
        if (desc == null)
            desc = "";

        presenter.updateGraphEle(i);
        descRoute.setText(desc);
        Route r = presenter.getRoute(i);
        lenRoute.setText(String.format(Locale.ENGLISH,"%9.3f m.",Decoder.getLen(r)));
        lenRoute.append("\n" + r.getTime().toString());
    }

    private RouteView routeView;
    @BindView(R.id.desc)
    protected TextView descRoute;
    @BindView(R.id.len)
    protected TextView lenRoute;
    private Presenter presenter;
    @BindView(R.id.graph)
    protected GraphView graphView;


    @OnClick(R.id.buttonAppend)
    public void appendRoute(Button button) {
        OpenFileDialog fileDialog = new OpenFileDialog(button.getContext())
                .setFileter(".*\\.gpx"+"|"+".*\\.txt")
                .setOpenDialogListener(new OpenFileDialog.OpenDialogListener() {
                    @Override
                    public void OnSelectedFile(String fileName) {
                        if (fileName.matches(".*\\.gpx"))
                            presenter.appendRoute(Parser.SAXParser(fileName));
                        if (fileName.matches(".*\\.txt"))
                            presenter.appendRoute(Parser.PolylineParser(fileName));
                    }
                });
        fileDialog.show();
    }

    @OnClick(R.id.buttonUndo)
    public void undoRoute(ImageButton button) {
        presenter.undoRoute();
    }

    @OnClick(R.id.buttonRedo)
    public void redoRoute(ImageButton button) {
        presenter.redoRoute();
    }

    @OnClick(R.id.buttonCreateRoute)
    public void createRoute(Button button) {
        Intent intent = new Intent(this, CreateRouteActivity.class);
        startActivityForResult(intent, REQUEST_CREATE);
    }

    private final int REQUEST_CREATE = 1;
    private final int REQUEST_ENCODE = 2;
    private final String TAG = "MainActivity";
    public int getRequestCreate() {
        return REQUEST_CREATE;
    }
    private int selectIndex = -1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        Log.d(TAG, "Create");


        presenter = new Presenter(this, App.storage());
        descRoute.setText("");
        lenRoute.setText("");

        final ArrayAdapter adapter = new ArrayAdapter<String>(this, R.layout.list_item);
        listView.setAdapter(adapter);
        routeView = new RouteView(adapter, presenter);
        registerForContextMenu(listView);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)menuInfo;
        selectIndex = info.position;
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.edit:
                String tableName = getResources().getString(R.string.stdTableName);
                createTableForRoute(tableName);

                Intent intent = new Intent(RouteActivity.this, PointActivity.class);
                intent.putExtra(getResources().getString(R.string.nameRoute), listView.getAdapter().getItem(selectIndex).toString());
                intent.putExtra(getResources().getString(R.string.tableName), tableName);
                intent.putExtra(getResources().getString(R.string.routeIndex), selectIndex);
                startActivity(intent);
                return true;
            case R.id.delete:
                Toast.makeText(RouteActivity.this, "Удаление", Toast.LENGTH_SHORT).show();
                presenter.deleteRoute(selectIndex);
                return true;
            case R.id.encode:
                Intent in = new Intent(this, PolylineActivity.class);
                in.putExtra(getResources().getString(R.string.routeIndex), selectIndex);
                startActivityForResult(in, REQUEST_ENCODE);
                return true;
            default:
                return super.onContextItemSelected(item);
        }


    }

    private void createTableForRoute(String tableName) {
        DBHelper dbHelper = new DBHelper(this);
        dbHelper.reBuild(tableName);
        presenter.getRoute(selectIndex).exportToDB(dbHelper, tableName);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case REQUEST_CREATE:
                processingRequestCreate(resultCode, data);
                break;
            case REQUEST_ENCODE:
                processingRequestEncode(resultCode, data);
                break;
        }
    }

    private void processingRequestCreate(int resultCode, Intent data) {
        switch (resultCode) {
            case RESULT_OK:
                Toast.makeText(this, getResources().getString(R.string.okResult), Toast.LENGTH_SHORT).show();
                Route r = new Route(data.getStringExtra(getResources().getString(R.string.nameRoute)), (Point[])null);
                r.setDesc(data.getStringExtra(getResources().getString(R.string.descRouteIntent)));
                presenter.appendRoute(r);
                break;
            case RESULT_CANCELED:
                Toast.makeText(this, getResources().getString(R.string.cancelResult), Toast.LENGTH_SHORT).show();
                break;
        }
    }
    private void processingRequestEncode(int resultCode, Intent data) {
        switch (resultCode) {
            case RESULT_OK:
                Toast.makeText(this, getResources().getString(R.string.saved), Toast.LENGTH_SHORT).show();
                descRoute.setText(data.getStringExtra(getResources().getString(R.string.filePath)+getResources().getString(R.string.fileName)));
                break;

            case RESULT_CANCELED:
                Toast.makeText(this, getResources().getString(R.string.cancelResult), Toast.LENGTH_SHORT).show();
                break;
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        graphView.setVisibility(View.INVISIBLE);
        Log.d(TAG, "Resume");

    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "Start");
//        Storage.getIntent().inform();
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG, "STOOOOP!");
    }

    @Override
    public void setEleGraph(Series s) {
        if (s != null) {
            graphView.removeAllSeries();
            graphView.addSeries(s);
            graphView.setVisibility(View.VISIBLE);
            Viewport viewport = graphView.getViewport();
            viewport.setXAxisBoundsManual(true);
            viewport.setMinX(0.0);
            viewport.setMaxX(100.0);
            viewport.setMinY(0.0);
        }
        else
            graphView.setVisibility(View.INVISIBLE);
    }

    @Override
    public void updatePointView() {
    }

    @Override
    public void updateRouteView() {
        routeView.update();
        listItemClick(presenter.getCountRoute()-1);
    }
}
