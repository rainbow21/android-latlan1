package root.ppo_lab1;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.PolyUtil;
import com.google.maps.android.SphericalUtil;

import java.util.ArrayList;

public class Decoder {
    public static String getPolyLine(Route r) {
        ArrayList list = new ArrayList();
        int n = r.getSize();
        for (int i = 0; i < n; i++) {
            Point p = r.getPoint(i);
            LatLng ll = new LatLng(p.getLat(), p.getLon());
            list.add(ll);
        }
        return PolyUtil.encode(list);
    }

    public static Route getRoute(String line) {
        ArrayList<LatLng> list = (ArrayList)PolyUtil.decode(line);
        ArrayList<Point> point = new ArrayList<>();

        int n = list.size();
        for (int i = 0; i < n; i++) {
            LatLng ll = list.get(i);
            point.add(new Point(Double.valueOf(ll.latitude), Double.valueOf(ll.longitude)));
        }
        return new Route(null, point);
    }
    public static Double getLen(Route r) {
        ArrayList<LatLng> path = new ArrayList<>();
        int n = r.getSize();
        for (int i = 0; i < n; i++) {
            Point p = r.getPoint(i);
            LatLng ll = new LatLng(p.getLat(), p.getLon());
            path.add(ll);
        }
        return SphericalUtil.computeLength(path);
    }
    public static Double getLen(LatLng p1, LatLng p2) {
        ArrayList<LatLng> path = new ArrayList<>();
        path.add(p1);
        path.add(p2);
        return SphericalUtil.computeLength(path);
    }

}
// Ключ к Google Map Android API
//AIzaSyA7tKmCpxCfbzO27sscNkox5DusoquyzaA

/*
20:A2:B9:54:D8:35:43:D6:11:72:A8:04:27:BB:24:7D:F7:3F:3A:1D
*/