package Queue;

import java.util.Enumeration;
import java.util.NoSuchElementException;

/**
 * Created by Igor on 11.03.2018.
 */
public final class FIFOQueueEnumerator<T> implements Enumeration<T> {
    Queue<T> queue;
    QueueElement<T> cursor;

    FIFOQueueEnumerator(Queue<T> var1) {
        this.queue = var1;
        this.cursor = var1.tail;
    }

    public boolean hasMoreElements() {
        return this.cursor != null;
    }

    public T nextElement() {
        Queue var1 = this.queue;
        synchronized(this.queue) {
            if(this.cursor != null) {
                QueueElement var2 = this.cursor;
                this.cursor = this.cursor.prev;
                return (T)var2.obj;
            }
        }

        throw new NoSuchElementException("FIFOQueueEnumerator");
    }
}