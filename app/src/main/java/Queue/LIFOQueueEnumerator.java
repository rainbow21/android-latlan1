package Queue;

import java.util.Enumeration;
import java.util.NoSuchElementException;

/**
 * Created by Igor on 11.03.2018.
 */
public final class LIFOQueueEnumerator<T> implements Enumeration<T> {
    Queue<T> queue;
    QueueElement<T> cursor;

    LIFOQueueEnumerator(Queue<T> var1) {
        this.queue = var1;
        this.cursor = var1.head;
    }

    public boolean hasMoreElements() {
        return this.cursor != null;
    }

    public T nextElement() {
        Queue var1 = this.queue;
        synchronized(this.queue) {
            if(this.cursor != null) {
                QueueElement var2 = this.cursor;
                this.cursor = this.cursor.next;
                return (T)var2.obj;
            }
        }

        throw new NoSuchElementException("LIFOQueueEnumerator");
    }
}

