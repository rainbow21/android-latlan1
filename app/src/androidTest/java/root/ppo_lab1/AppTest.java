package root.ppo_lab1;

import android.content.Context;
import android.util.Log;
import android.view.ContextThemeWrapper;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;

import java.util.ArrayList;

import Command.Command;
import Command.StorageAppendRouteCommand;

public class AppTest {
    private Decoder decoder;
    private Context context;
    final private int countRoute = 5;
    final private String TAG = "TEST";

    @Before
    public void init() {
//        decoder.setRes(context.getResources());
        context = new ContextThemeWrapper();
    }

    @After
    public void post() {
        Storage.destroyIntent();
    }


    @Test
    public void testEncodeDecode() {
        Route r = new Route("Name", (ArrayList<Point>) null);
        Assert.assertEquals(r, Decoder.getRoute(Decoder.getPolyLine(r)));

        r.appendPoint(new Point(0.0, 0.0, 0.0));
        Assert.assertEquals(r, Decoder.getRoute(Decoder.getPolyLine(r)));

        r.appendPoint(new Point(0.0, 0.1, 0.2));
        Assert.assertEquals(r, Decoder.getRoute(Decoder.getPolyLine(r)));
    }


    public void start() {
        JUnitCore core = new JUnitCore();
        Result result = core.run(AppTest.class);
        Log.d(TAG, "run tests: " + result.getRunCount());
        Log.d(TAG, "failder tests: " + result.getFailureCount());
    }
}
