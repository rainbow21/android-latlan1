package root.ppo_lab1;


import org.junit.Test;

import java.util.ArrayList;
import java.util.Iterator;

import API.Presenter;
import API.ViewAPI;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class Test2 {

    @Test
    public void test1() {
        assertEquals(1, 1);

        Iterator iter = mock(Iterator.class);
        when(iter.next()).thenReturn(1).thenReturn(2).thenReturn(3);

        Storage storage = mock(Storage.class);
        when(storage.getRoute(anyInt())).thenReturn(null);

        assertEquals(1, iter.next());
        assertEquals(2, iter.next());
        assertEquals(3, iter.next());
        assertEquals(3, iter.next());
    }

    // Тест на высоту
    @Test
    public void testCorrectEle() {
        Route route = new Route("R1", new ArrayList<Point>());
        assertEquals(false, route.isReadyEle());

        route.appendPoint(new Point(0.0, 0.0, 0.0));
        route.appendPoint(new Point(0.0, 1.0));
        assertEquals(false, route.isReadyEle());
        assertEquals(true, route.getProcEle() <= 50);

        route.appendPoint(new Point(0.0, 1.5, 2.0));
        assertEquals(false, route.isReadyEle());
        assertEquals(true, route.getProcEle() >= 50);

        route.getPoint(1).setEle(1.0);
        assertEquals(true, route.isReadyEle());
    }

    // Импорт маршрута из полилайна
    // Конвертация в полилайн
    @Test
    public void testImportRouteFromPolyline() {
        Route r = new Route("R1", new ArrayList<Point>());
        r.appendPoint(new Point(0.0, 0.0));
        r.appendPoint(new Point(0.1, 0.1));

        final String pol = "??_pR_pR";
        assertEquals(pol, Decoder.getPolyLine(r));

        Route o = Decoder.getRoute(pol);
        int n = o.getSize();
        for (int i = 0; i < n; i++) {
            Point p1 = r.getPoint(i);
            Point p2 = o.getPoint(i);
            assertEquals(true, p1.toString().equals(p2.toString()));
        }

        final String empty = "";
        Route route = Decoder.getRoute(empty);
        assertEquals(0, route.getSize());

        // Длина
        Route r2 = new Route("R2", new ArrayList<Point>());
        for (int i = 0; i < 5; i++)
            r2.appendPoint(new Point(i*1.0, i*1.0));
        Double len = Decoder.getLen(r2);
        String pl = Decoder.getPolyLine(r2);
        Route newR = Decoder.getRoute(pl);
        Double len2 = Decoder.getLen(newR);
        assertEquals(true, Math.abs(len-len2) < 0.001);
    }

    // Редактирование точки
    @Test
    public void testEditPoint() {
        Presenter presenter = mock(Presenter.class);
        Route route = new Route("R1", new ArrayList<Point>());
        when(presenter.getRoute(anyInt())).thenReturn(route);
        // Добавим точку без высоты
        presenter.getRoute(5999).appendPoint(new Point(0.0, 0.0));
        assertEquals(true, presenter.getRoute(0).getPoint(0).getEle() == null);
        // Добавим точку с высотой
        presenter.getRoute(677).editPoint(0, new Point(1.0, 1.0, 1.0));
        assertEquals(1, presenter.getRoute(0).getSize());
        assertEquals(new Point(1.0, 1.0, 1.0).toString(), presenter.getRoute(0).getPoint(0).toString());

        presenter.getRoute(-101).editPoint(0, new Point(2.0, 2.0, 2.0));
        assertEquals(1, presenter.getRoute(0).getSize());
        assertEquals(new Point(2.0, 2.0, 2.0).toString(),
                     presenter.getRoute(0).getPoint(0).toString());
    }

    // Удаление/Создание точки
    @Test
    public void testAppend_DeletePoint() {
        Presenter presenter = mock(Presenter.class);
        Route route = new Route("R1", new ArrayList<Point>());
        when(presenter.getRoute(anyInt())).thenReturn(route);
        int n = 5;
        for (int i = 0; i < n; i++)
            presenter.getRoute(99).appendPoint(new Point(1.0 * i, 1.0 * i));
        assertEquals(n, presenter.getRoute(0).getSize());

        for (int i = 0; i < n-1; i++)
            presenter.getRoute(99).deletePoint(0);
        assertEquals(1, presenter.getRoute(0).getSize());

        // Остаться должна была последняя точка. 1.0 * (n-1)
        assertEquals(new Point(1.0*(n-1), 1.0*(n-1)).toString(), presenter.getRoute(999).getPoint(0).toString());
    }

    // Создание/Удаление маршрута маршрута
    @Test
    public void testCreateRoute() {
        ViewAPI viewAPI = mock(ViewAPI.class);
        Storage storage = new Storage();
        Presenter presenter = new Presenter(viewAPI, storage);

        presenter.appendRoute(new Route("R1", new ArrayList<Point>()));
        assertEquals(1, presenter.getCountRoute());

        int n = 5;
        for (int i = 0; i < n; i++)
            presenter.appendRoute(new Route(Integer.valueOf(i).toString(), new ArrayList<Point>()));
        assertEquals(n+1, presenter.getCountRoute());

        for (int i = 0; i < n; i++)
            presenter.deleteRoute(presenter.getCountRoute()-1);
        assertEquals(1, presenter.getCountRoute());
        // Останется единственный маршрут
        assertEquals("R1", presenter.getRoute(0).getName());

        presenter.deleteRoute(0);
        assertEquals(0, presenter.getCountRoute());
    }
}
