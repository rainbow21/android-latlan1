package root.ppo_lab1;

import org.junit.Test;

import java.util.ArrayList;

import Command.Command;
import Command.StorageAppendRouteCommand;
import Command.StorageDeleteRouteCommand;

import static org.junit.Assert.assertEquals;

public class ivTest {
    @Test
    public void testAppend1() {
        Storage storage = new Storage();
        Route r = new Route("r1", (ArrayList<Point>)null);
        Command cmd = StorageAppendRouteCommand.getBuilderAppendRouteCommand()
                .buildStorage(storage)
                .buildRoute(r)
                .build();
        storage.getControllerEditRoute().exec(cmd);

        assertEquals(storage.getControllerEditRoute().getHistorySize(), 1);
        assertEquals(storage.getControllerEditRoute().getPullSize(), 0);
        assertEquals(storage.getCountRoute(), 1);
        assertEquals(storage.getRoute(0), r);
    }

    @Test
    public void testAppend() {
        Storage storage = new Storage();
        Route r[] = {
                new Route("r1", (ArrayList<Point>)null),
                new Route("r2", (ArrayList<Point>)null),
                new Route("r3", (ArrayList<Point>)null),
                new Route("r4", (ArrayList<Point>)null),
        };
        final int count = r.length;
        for (int i = 0; i < count; i++) {
            Command cmd = StorageAppendRouteCommand.getBuilderAppendRouteCommand()
                    .buildStorage(storage)
                    .buildRoute(r[i])
                    .build();
            storage.getControllerEditRoute().exec(cmd);
        }

        assertEquals(storage.getControllerEditRoute().getHistorySize(), count);

        for (int i = 0; i < count; i++) {
            assertEquals(storage.getRoute(0).getName(), r[i].getName());
            Command cmd = StorageDeleteRouteCommand.getBuilderStorageDeleteCommand()
                    .buildStorage(storage)
                    .buildRoute(0)
                    .build();
            storage.getControllerEditRoute().exec(cmd);
        }
        assertEquals(storage.getControllerEditRoute().getHistorySize(), count*2);

    }

    @Test
    public void testDecode() {
        Route r = new Route(null, (ArrayList<Point>)null);
        r.appendPoint(new Point(Double.valueOf(0.0), Double.valueOf(0.0)));
        r.appendPoint(new Point(Double.valueOf(0.1), Double.valueOf(0.1)));

        final String pol = "??_pR_pR";

        assertEquals(pol, Decoder.getPolyLine(r));
        Route o = Decoder.getRoute(pol);
        int n = o.getSize();
        for (int i = 0; i < n; i++) {
            Point p1 = r.getPoint(i);
            Point p2 = o.getPoint(i);
            assertEquals(true, p1.toString().equals(p2.toString()));
        }
    }
}
